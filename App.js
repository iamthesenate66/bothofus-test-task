import React from 'react';

import { createStackNavigator,
    createAppContainer,
} from 'react-navigation';

import Main from './src/components/Main'

const AppStackNavigator = createStackNavigator({
    Main,
}, {
    initialRouteName: 'Main',
});

const AppContainer = createAppContainer(AppStackNavigator);

export default AppContainer;
