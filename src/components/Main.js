import React from 'react';

import {
    SafeAreaView,
    Text,
    StyleSheet,
    TouchableOpacity,
    View,
    Image,
    TextInput,
    FlatList,
} from 'react-native';

import IconIon from 'react-native-vector-icons/Ionicons';
import Reply from './Reply';

class Main extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: (
                <Text
                    numberOfLines={1}
                    style={{
                        fontFamily: "Roboto-Bold",
                        fontSize: 16,
                        lineHeight: 19,
                        letterSpacing: 0,
                        color: '#000',
                    }}
                >Hur Fungerar Det Med Bostadsk Om Man Ska Byta Till Lunds Universitet</Text>
            ),
            headerLeft: (
                <TouchableOpacity onPress={() => navigation.goBack(null)}>
                    <IconIon style={{marginLeft: 20}} name='ios-arrow-back' color='#000' size={24}/>
                </TouchableOpacity>
            ),
        }
    };

    state = {
        repliesData: [
            {
                id: '123',
                avatar: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                reply: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut mollis lacus in eros blandit laoreet. Vivamus ac lacinia ipsum. Donec sodales congue arcu. Pellentesque lobortis eleifend ligula ac commodo. Vestibulum eget porta sem, vitae volutpat quam. Sed blandit viverra euismod. Aenean nec velit velit. Morbi congue nibh ultricies ligula malesuada.',
                upvotes: '32',
                downvotes: '2',
                isVoted: false,
            }, {
                id: '1234',
                avatar: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                reply: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut mollis sem, vitae volutpat quam. Sed blandit viverra euismod. Aenean nec velit velit. Morbi congue nibh ultricies ligula malesuada.',
                upvotes: '12',
                downvotes: '40',
                isVoted: false,
            },
        ],
    };

    onReplyVote = (id, vote) => {
        const chosenItem = this.state.repliesData.findIndex((elem) => (elem.id === id));
        if(this.state.repliesData[chosenItem].isVoted) {
            return null;
        }
        this.setState(prevState => {
            return {
                repliesData: prevState.repliesData.map((elem, index) => {
                    if(index === chosenItem) {
                        return {
                            ...elem,
                            [vote ? 'upvotes' : 'downvotes']: + elem[vote ? 'upvotes' : 'downvotes'] + 1,
                            isVoted: true,
                        }
                    }
                    return elem;

                })
            }
        });
    };

    render() {
        return (
            <SafeAreaView style={s.appContainer}>
                <View style={s.mainTopicContainer}>
                    <Text style={s.mainTopic}>Hur fungerar det med bostadsk om man ska byta till lunds universitet?</Text>
                    <View style={s.senderContainer}>
                        <Text style={s.fromTxt}>Av: </Text>
                        <View style={s.senderInfo}>
                            <Image style={s.senderAvatar} source={{uri: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'}}/>
                            <Text style={s.senderName}>Guillermo Francis</Text>
                        </View>
                    </View>
                    <Text style={s.topicMessage}>
                        Tjenna!

                        Hur funkar det? Tnker Att Det Borde Finnas Lite Olika Tricks FrAtt FEtt Schysst Boende Ngon Som Har Ngra Tips
                    </Text>
                </View>
                <View style={s.inputContainer}>
                    <Image style={s.inputAvatar} source={{uri: 'https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'}}/>
                    <TextInput
                        placeholder='Skriv svar...'
                        style={s.inputField}

                    />
                </View>
                <TouchableOpacity style={s.submitButton}>
                    <IconIon name='md-send' color='#fff' size={16}/>
                    <Text style={s.submitTxt}>Skicka</Text>
                </TouchableOpacity>
                <FlatList
                    style={s.repliesList}
                    keyExtractor={(item) => item.id}
                    data={this.state.repliesData}
                    renderItem={({ item }) => (
                        <Reply
                            data={item}
                            onReplyVote={this.onReplyVote}
                        />
                    )}
                />
            </SafeAreaView>
        );
    }
}

const s = StyleSheet.create({
    appContainer: {
        alignItems: 'center',
        paddingHorizontal: 10,
        backgroundColor: '#eee',
        flex: 1,
    },
    mainTopicContainer: {
        width: '100%',
        borderRadius: 14,
        backgroundColor: '#fff',
        elevation: 1,
        shadowColor: '#888',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowRadius: 5,
        shadowOpacity: 1,
        padding: 20,
        marginTop: 20,
    },
    mainTopic: {
        fontFamily: 'Roboto-Bold',
        fontSize: 16,
        letterSpacing: 0,
        color: '#000',
        lineHeight: 19,
    },
    senderContainer: {
        flexDirection: 'row',
        height: 44,
        alignItems: 'center',
    },
    fromTxt: {
        fontFamily: 'Roboto',
        fontSize: 14,
        color: '#888',
        marginRight: 10,
    },
    senderInfo: {
        flexDirection: 'row',
    },
    senderAvatar: {
        width: 20,
        height: 20,
        borderRadius: 10,
        marginRight: 10,
    },
    senderName: {
        fontFamily: 'Roboto',
        color: '#888',
    },
    topicMessage: {
        fontFamily: 'Roboto',
        lineHeight: 17,
        fontSize: 14,
        color: '#888',
    },
    inputContainer: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 10,
    },
    inputAvatar: {
        width: 33,
        height: 33,
        borderRadius: 16.5,
        marginRight: 10,
    },
    inputField: {
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
        height: '100%',
        flex: 1,
    },
    submitButton: {
        alignSelf: 'flex-end',
        backgroundColor: '#048DEA',
        flexDirection: 'row',
        height: 36,
        borderRadius: 18,
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    submitTxt: {
        color: '#fff',
        fontSize: 14,
        fontFamily: 'Roboto',
        marginLeft: 10,
    },
    repliesList: {
        width: '100%',
    },
});

export default Main;
