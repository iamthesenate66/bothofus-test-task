import React from 'react';
import { Image,
    Text,
    TouchableOpacity,
    View,
    StyleSheet,
} from 'react-native';
import IconIon from 'react-native-vector-icons/Ionicons';

const Reply = ({ data, onReplyVote }) => (
    <View style={s.replyContainer}>
        <Image style={s.replyAvatar} source={{uri: data.avatar}}/>
        <View style={s.replyBox}>
            <Text style={s.replyTxt}>
                { data.reply }
            </Text>
            <View style={s.replyStats}>
                <TouchableOpacity style={s.replyVoteButton} onPress={() => onReplyVote(data.id, true)}>
                    <IconIon color='#666' size={16} name='md-arrow-up'/>
                    <Text style={s.voteTxt}>{`Rosta upp (${data.upvotes})`}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={s.replyVoteButton} onPress={() => onReplyVote(data.id, false)}>
                    <IconIon color='#666' size={16} name='md-arrow-down'/>
                    <Text style={s.voteTxt}>{`Rosta ner (${data.downvotes})`}</Text>
                </TouchableOpacity>
            </View>
        </View>
    </View>
);

export default Reply;

const s = StyleSheet.create({
    replyContainer: {
        flexDirection: 'row',
        width: '100%',
        marginTop: 20,
    },
    replyAvatar: {
        width: 43,
        height: 43,
        borderRadius: 21.5,
        marginRight: 15,
    },
    replyBox: {
        flex: 1,
        borderRadius: 14,
        backgroundColor: '#fff',
        elevation: 1,
        shadowColor: '#888',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowRadius: 5,
        shadowOpacity: 1,
        padding: 20,
        marginRight: 5,
    },
    replyTxt: {
        fontFamily: 'Roboto',
        fontSize: 14,
        lineHeight: 17,
        color: '#666',
    },
    replyStats: {
        flexDirection: 'row',
        marginTop: 15,
    },
    replyVoteButton: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 25,
    },
    voteTxt: {
        color: '#666',
        fontSize: 12,
        marginLeft: 10,
    },
});
